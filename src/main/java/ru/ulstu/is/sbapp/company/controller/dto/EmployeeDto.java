package ru.ulstu.is.sbapp.company.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.ulstu.is.sbapp.company.model.Employee;

import javax.validation.constraints.NotBlank;
import java.util.*;
import java.util.stream.Collectors;

public class EmployeeDto {
    private Long id;
    @NotBlank(message = "Firstname can't be null or empty")
    private String firstName;
    @NotBlank(message = "Lastname can't be null or empty")
    private String lastName;
    private PositionDto position;
    private List<EmployeeDepartmentDto> departments;

    public EmployeeDto(){

    }
    public EmployeeDto(Employee employee){
        this.id = employee.getId();
        this.firstName=employee.getFirstName();
        this.lastName=employee.getLastName();
        if(employee.getPosition()!=null){
            this.position=new PositionDto(employee.getPosition());
        }
        this.departments = employee.getDepartments().stream()
                .map(EmployeeDepartmentDto::new)
                .toList();
    }
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName(){ return lastName; }

    public PositionDto getPosition(){
        return position;
    }

    public List<EmployeeDepartmentDto> getDepartments() {
        return departments;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPosition(PositionDto position) {
        this.position = position;
    }

    public void setDepartments(List<EmployeeDepartmentDto> departments) {
        this.departments = departments;
    }

    public String prettyDepartments(){
        return departments.stream()
                .map(EmployeeDepartmentDto::getName)
                .collect(Collectors.joining(", "));
    }
}
