package ru.ulstu.is.sbapp.company.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.company.controller.dto.*;
import ru.ulstu.is.sbapp.company.service.DepartmentService;
import ru.ulstu.is.sbapp.company.service.EmployeeService;

import javax.validation.Valid;

@Controller
@RequestMapping("/department")
public class DepartmentMvcController {
    private final EmployeeService employeeService;
    private final DepartmentService departmentService;
    public DepartmentMvcController(EmployeeService employeeService, DepartmentService departmentService){
        this.employeeService=employeeService;
        this.departmentService=departmentService;
    }
    @GetMapping
    public String getDepartment(Model model) {
        model.addAttribute("departments",
                departmentService.findAllDepartments().stream()
                        .map(DepartmentDto::new)
                        .toList());
        return "department";
    }

    @GetMapping(value = {"/edit", "/edit/{id}"})
    public String editDepartment(@PathVariable(required = false) Long id,
                               Model model) {
        model.addAttribute("employees",employeeService.findAllEmployees().stream()
                .map(DepartmentEmployeeDto::new)
                .toList());
        if (id == null || id <= 0) {
            model.addAttribute("departmentDto", new DepartmentDto());
        } else {
            model.addAttribute("departmentId", id);
            model.addAttribute("departmentDto", new DepartmentDto(departmentService.findDepartment(id)));
        }
        return "department-edit";
    }

    @PostMapping(value = {"", "/{id}"})
    public String saveDepartment(@PathVariable(required = false) Long id,
                               @ModelAttribute @Valid DepartmentDto departmentDto,
                               BindingResult bindingResult,
                               Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", bindingResult.getAllErrors());
            return "department-edit";
        }
        if (id == null || id <= 0) {
            departmentService.addDepartment(departmentDto);
        } else {
            departmentService.updateDepartment(departmentDto);
        }

        return "redirect:/department";
    }

    @PostMapping("/delete/{id}")
    public String deleteDepartment(@PathVariable Long id) {
        departmentService.deleteDepartment(id);
        return "redirect:/department";
    }
}
