package ru.ulstu.is.sbapp.company.controller.dto;

import ru.ulstu.is.sbapp.company.model.Department;

import java.util.List;
import java.util.stream.Collectors;

public class DepartmentDto {
    private Long id;
    private String name;
    private List<DepartmentEmployeeDto> employees;

    public DepartmentDto(){

    }
    public DepartmentDto(Department department){
        this.id= department.getId();
        this.name= department.getName();
        this.employees = department.getEmployees().stream()
                .map(DepartmentEmployeeDto::new)
                .toList();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<DepartmentEmployeeDto> getEmployees() {
        return employees;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmployees(List<DepartmentEmployeeDto> employees) {
        this.employees = employees;
    }

    public String prettyEmployees(){
        return employees.stream()
                .map(DepartmentEmployeeDto::getName)
                .collect(Collectors.joining(", "));
    }
}
