package ru.ulstu.is.sbapp.company.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.company.controller.dto.EmployeeDepartmentDto;
import ru.ulstu.is.sbapp.company.controller.dto.EmployeeDto;
import ru.ulstu.is.sbapp.company.controller.dto.PositionDto;
import ru.ulstu.is.sbapp.company.model.Department;
import ru.ulstu.is.sbapp.company.model.Position;
import ru.ulstu.is.sbapp.company.service.DepartmentService;
import ru.ulstu.is.sbapp.company.service.EmployeeService;
import ru.ulstu.is.sbapp.company.service.PositionService;
import ru.ulstu.is.sbapp.student.controller.StudentDto;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/employee")
public class EmployeeMvcController {
    private final EmployeeService employeeService;
    private final PositionService positionService;
    private final DepartmentService departmentService;
    public EmployeeMvcController(EmployeeService employeeService, PositionService positionService, DepartmentService departmentService){
        this.employeeService=employeeService;
        this.positionService=positionService;
        this.departmentService=departmentService;
    }
    @GetMapping
    public String getEmployees(Model model) {
        model.addAttribute("employees",
                employeeService.findAllEmployees().stream()
                        .map(EmployeeDto::new)
                        .toList());
        return "employee";
    }

    @GetMapping(value = {"/edit", "/edit/{id}"})
    public String editEmployee(@PathVariable(required = false) Long id,
                              Model model) {
        model.addAttribute("positions", positionService.findAllPositions().stream()
                .map(PositionDto::new)
                .toList());
        model.addAttribute("departments",departmentService.findAllDepartments().stream()
                .map(EmployeeDepartmentDto::new)
                .toList());
        if (id == null || id <= 0) {
            model.addAttribute("employeeDto", new EmployeeDto());
        } else {
            model.addAttribute("employeeId", id);
            model.addAttribute("employeeDto", new EmployeeDto(employeeService.findEmployee(id)));
        }
        return "employee-edit";
    }

    @PostMapping(value = {"", "/{id}"})
    public String saveEmployee(@PathVariable(required = false) Long id,
                              @ModelAttribute @Valid EmployeeDto employeeDto,
                              BindingResult bindingResult,
                              Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", bindingResult.getAllErrors());
            return "employee-edit";
        }
        if (id == null || id <= 0) {
            employeeService.addEmployee(employeeDto);

        } else {
            employeeService.updateEmployee(employeeDto);
        }

        return "redirect:/employee";
    }

    @PostMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable Long id) {
        employeeService.deleteEmployee(id);
        return "redirect:/employee";
    }
}
