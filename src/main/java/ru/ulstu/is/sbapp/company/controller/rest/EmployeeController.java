package ru.ulstu.is.sbapp.company.controller.rest;

import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.WebConfiguration;
import ru.ulstu.is.sbapp.company.controller.dto.EmployeeDto;
import ru.ulstu.is.sbapp.company.service.EmployeeService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(WebConfiguration.REST_API + "/employee")
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/{id}")
    public EmployeeDto getEmployee(@PathVariable Long id) {
        return new EmployeeDto(employeeService.findEmployee(id));
    }

    @GetMapping
    public List<EmployeeDto> getEmployees() {
        return employeeService.findAllEmployees().stream()
                .map(EmployeeDto::new)
                .toList();
    }

    @PostMapping
    public EmployeeDto createEmployee(@RequestBody @Valid EmployeeDto employeeDto) {
        //return new EmployeeDto(employeeService.addEmployee(employeeDto.getFirstName(),
         //       employeeDto.getLastName()));
        return new EmployeeDto();
    }

    @DeleteMapping("/{id}")
    public EmployeeDto deleteEmployee(@PathVariable Long id) {
        return new EmployeeDto(employeeService.deleteEmployee(id));
    }


    @PutMapping("/{id}")
    public EmployeeDto updateEmployee(@RequestBody @Valid EmployeeDto employeeDto) {
        return employeeService.updateEmployee(employeeDto);
    }

}
