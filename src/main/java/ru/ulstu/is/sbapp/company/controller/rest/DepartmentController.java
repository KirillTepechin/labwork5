package ru.ulstu.is.sbapp.company.controller.rest;

import org.springframework.web.bind.annotation.*;

import ru.ulstu.is.sbapp.WebConfiguration;
import ru.ulstu.is.sbapp.company.controller.dto.DepartmentDto;
import ru.ulstu.is.sbapp.company.service.DepartmentService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(WebConfiguration.REST_API+"/department")
public class DepartmentController {

    private final DepartmentService departmentService;

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping("/{id}")
    public DepartmentDto getDepartment(@PathVariable Long id) {
        return new DepartmentDto(departmentService.findDepartment(id));
    }

    @GetMapping("/")
    public List<DepartmentDto> getDepartments() {
        return departmentService.findAllDepartments().stream()
                .map(DepartmentDto::new)
                .toList();
    }

    @PostMapping("/")
    public DepartmentDto createDepartment(@RequestParam("name") String name) {
        //return new DepartmentDto(departmentService.addDepartment(name));
        return new DepartmentDto();
    }

    @PutMapping("/{id}")
    public DepartmentDto updateDepartment(@RequestBody @Valid DepartmentDto departmentDto) {
        return departmentService.updateDepartment(departmentDto);
    }

    @DeleteMapping("/{id}")
    public DepartmentDto deleteDepartment(@PathVariable Long id) {
        return new DepartmentDto(departmentService.deleteDepartment(id));
    }

}
