package ru.ulstu.is.sbapp.company.controller.dto;

import ru.ulstu.is.sbapp.company.model.Department;

public class EmployeeDepartmentDto {
    private Long id;
    private String name;

    public EmployeeDepartmentDto() {

    }

    public EmployeeDepartmentDto(Department department) {
        this.id = department.getId();
        this.name = department.getName();
    }

    public long getId() {
        return id;
    }
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName();
    }
}
