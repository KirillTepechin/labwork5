package ru.ulstu.is.sbapp.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.ulstu.is.sbapp.company.controller.dto.DepartmentEmployeeDto;
import ru.ulstu.is.sbapp.company.controller.dto.EmployeeDepartmentDto;
import ru.ulstu.is.sbapp.company.service.DepartmentService;
import ru.ulstu.is.sbapp.company.service.EmployeeService;

@Component
public class EmployeeByIdConverter implements Converter<String, DepartmentEmployeeDto> {

    private EmployeeService employeeService;

    @Autowired
    public EmployeeByIdConverter(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Override
    public DepartmentEmployeeDto convert(String id) {
        return new DepartmentEmployeeDto(employeeService.findEmployee(Long.parseLong(id)));
    }
}
